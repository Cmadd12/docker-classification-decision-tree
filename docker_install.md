Please refernce the official docker installation documantion.

https://docs.docker.com/docker-for-mac/install/


1. Double-click Docker.dmg to open the installer, then drag Moby the whale to the Applications folder.

2. Double-click Docker.app in the Applications folder to start Docker. 
3. You are prompted to authorize Docker.app with your system password after you launch it. Privileged access is needed to install networking components and links to the Docker apps.

    - The whale in the top status bar indicates that Docker is running, and accessible from a terminal.

<img src="resources/Screen_Shot_2019-03-02_at_12.13.10_PM.png" alt="drawing" width="350"/>

4. Open up a terminal and run the following command:
```
docker --version
```
    - You should see a similar output 
<img src="resources/Screen_Shot_2019-03-02_at_12.19.20_PM.png" alt="drawing" width="350"/>

5. Now run the following command which is the Hello World of Docker: 
```
docker run hello-world
```
    - You should see a similar output 
<img src="resources/Screen_Shot_2019-03-02_at_12.20.28_PM.png" alt="drawing" width="350"/>

### *Congratulations Docker is installed!*