# Create Docker Container

1. In your terminal run the following command to create your docker container where you will perform your analysis.

```
docker run --rm -p 10000:8888 -e JUPYTER_ENABLE_LAB=yes -v "$PWD":/home/maddco/work jupyter/datascience-notebook:9b06df75e445
```

2. Once all dependencies have been downloaded and installed visit your http://localhost:10000/ in your preffered web browser.
    - Please note if prompted for credentials to log into Jupyter notebook visit your temrinal and with your prior docker run command in step one. Look for a message that says "Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:". Use the token after this statement and paste it into the password prompt.

<img src="resources/Screen_Shot_2019-03-02_at_12.49.39_PM.png" alt="drawing" width="350"/>