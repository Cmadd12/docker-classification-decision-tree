# Docker Classification Decision Tree


- This project is going to consist of installing docker, creating a jupyter notebook container, development of decison tree algorthim and finally using VScode to visualize the decison tree.

- The machine learning decsion tree uses classification to look at sex and age to predict what type of music people prefer.


Please follow these steps:

1. Install docker
2. Create docker container
3. Access container and run python algorthim

    - Use music.csv file (provided)
4. Install VScode and required extentions for dot file
5. Utilize output file and visualize your decision tree

    - Use music-recommender.dot file (provided)

#### *Thank you for the inspiration Mosh*