# VScode Install

1. Please visit [this](https://code.visualstudio.com/download) site and select the appropriate download for your operqting system.

2. Open up VScode. Click the "Extentions Marketplace Button" to pull in external dependency to view a decision tree graphically in VSCode.

3. Filter down to extentions by searching on "dot"

<img src="resources/Screen_Shot_2019-03-02_at_1.34.26_PM.png" alt="drawing" width="350"/>

4. Install the extention and close VScode completely.

5. Open up VSCode and drag over your output file from the Python notebook "music-recommender.dot".

<img src="resources/Screen_Shot_2019-03-02_at_1.42.20_PM.png" alt="drawing" width="350"/>

6. Once the file is open use the following key stroker to preview the classification decision tree:

Windows:
```
ctrl + k + v
```
Mac:
```
cmd+k shift+v
```

<img src="resources/Screen_Shot_2019-03-02_at_1.42.49_PM.png" alt="drawing" width="350"/>

#### *Great job you have successfully visualized a decision tree!*